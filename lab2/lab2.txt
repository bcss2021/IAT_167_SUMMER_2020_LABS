void draw_grid() {
  pushStyle();
  stroke(0,0,0);
  fill(0,0,0);
  strokeWeight(1);
  textAlign(CENTER,CENTER);
  int step = 50;
  for(int i = 0; i < width/step; i++) {
    line(i*step, 0, i*step, height);
    if (i>0) {
      text(str(i*step), i*step, step);
    }
  }
  for (int i=0; i < height/step; i++) {
    line(0, i*step, width, i*step);
    if (i>0) {
      text(str(i*step), step, i*step);
    }
  }
  textAlign(LEFT,BOTTOM);
  popStyle();
}

void setup() {
  size(600,600); 
}

int grass_height = 400;
int loop = 20;

void draw() {
  
  int xpos = (int)map(abs(loop/2 - (frameCount % loop)), 0, loop/2, 0, width);
  float angle = radians(map(abs(loop/2 - (frameCount % loop)), 0, loop/2, 90, 270));
  float size = map(abs(loop/2 - (frameCount % loop)), 0, loop/2, 1, 1.5);
  
}
